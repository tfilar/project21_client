package com.client;
/**
 *
 * @Created by Tobiasz Filar, Artur Górowski, Krystian Lisowski
 */

import com.client.model.ClientFrame;
import javax.swing.*;

public class Main {
    public static void main(String[] args)  {
        JFrame frame = new JFrame();
        frame.setContentPane(new ClientFrame().getMainPanel());
        frame.pack();
        frame.setVisible(true);
        frame.setSize(500,400);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
