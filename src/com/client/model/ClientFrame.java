package com.client.model;

import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class ClientFrame {
    private JComboBox recipientBox;
    private JTextField titleField;
    private JTextArea contentArea;
    private JButton sendButton;
    private JLabel recipientLabel;
    private JLabel titleLabel;
    private JPanel mainPanel;

    public ClientFrame() {
        setContacts();
        sendButton.addActionListener(event -> sendButtonActionPerformed());
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void sendButtonActionPerformed() {
        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/email/send");

        final Gson gson = new Gson();

        final UserData userData = new UserData(recipientBox.getSelectedItem().toString(),
                titleField.getText().toString(), contentArea.getText().toString());
        newContact(userData);

        final String json = gson.toJson(userData);

        try {
            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);
            if(200 == response.getStatusLine().getStatusCode()) {
                JOptionPane.showMessageDialog(null, "Wiadomość wysłana!",
                        "Wyslano", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Nie udalo się wysłać wiadomości!",
                        "Bład", JOptionPane.ERROR_MESSAGE);
            }
            client.close();
            setComponentText();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void newContact(UserData userData) {
        for(int count = 0; count < recipientBox.getItemCount(); count++) {
            if(recipientBox.getItemAt(count).toString() != userData.getRecipient()) {
                if((count+1) == recipientBox.getItemCount()) {
                    recipientBox.addItem(userData.getRecipient());
                } else {
                    continue;
                }
            } else {
                break;
            }
        }
    }
    private void setContacts() {
        final HttpClient client = HttpClientBuilder.create().build();
        final HttpGet request = new HttpGet("http://127.0.0.1:8080/email/contacts");
        final HttpResponse response;
        final Gson gson = new Gson();
        try {
            response = client.execute(request);
            final HttpEntity entity = response.getEntity();
            final String json = EntityUtils.toString(entity);
            final ContactsJson contactsJson = gson.fromJson(json, ContactsJson.class);
            for (Recipient r : contactsJson.getContacts())
                recipientBox.addItem(r.getRecipient().toString());
            enabledButton();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void enabledButton() {
        sendButton.setEnabled(true);
        titleField.setEnabled(true);
        contentArea.setEnabled(true);
    }

    private void setComponentText() {
        titleField.setText("");
        contentArea.setText("");
        recipientBox.setToolTipText("");
    }

}
