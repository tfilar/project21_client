package com.client.model;

import com.client.Excepction.NoRecipientExcepction;

public class Recipient {
    private String recipient;

    public Recipient(String recipient) {
        if(recipient == null) {
            throw new NoRecipientExcepction("Nie podano adresu");
        }
        this.recipient = recipient;
    }

    /**
     *
     * @return the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    @Override
    public String toString() {
        return "Recipient{" +
                "recipient='" + recipient + '\'' +
                '}';
    }
}
