package com.client.model;

import java.io.Serializable;
import java.util.List;

public class ContactsJson implements Serializable{
    List<Recipient> contacts;

    public ContactsJson() {
    }

    /**
     *
     * @return the contacts
     */
    public List<Recipient> getContacts() {
        return contacts;
    }

    @Override
    public String toString() {
        return "ContactsJson{" +
                "contacts=" + contacts +
                '}';
    }
}