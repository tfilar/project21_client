package com.client.model;

import java.io.Serializable;

public class UserData implements Serializable {
    String recipient;
    String title;
    String content;

    public UserData() {

    }

    public UserData(String recipient, String title, String content) {
        this.recipient = recipient;
        this.title = title;
        this.content = content;
    }

    /**
     *
     * @return the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "recipient='" + recipient + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}